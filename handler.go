package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/labstack/echo"
)

// ESLogger function for Elastic Search log handler
func ESLogger(c echo.Context) error {
	body := echo.Map{}
	if err := c.Bind(&body); err != nil {
		return err
	}

	body["@timestamp"] = time.Now().Format(time.RFC3339)

	indexPattern := os.Getenv("INDEX_PATTERN")

	if indexPattern == "" {
		indexPattern = "kong-2006-01-02"
	}

	currentTime := time.Now().Format(indexPattern)

	esHost := os.Getenv("ES_HOST")
	if esHost == "" {
		esHost = "127.0.0.1"
	}

	esPort := os.Getenv("ES_PORT")
	if esPort == "" {
		esPort = "9200"
	}

	esUsername := os.Getenv("ES_USER")
	if esUsername == "" {
		esUsername = "demo"
	}
	esPassword := os.Getenv("ES_PASSWORD")
	if esPassword == "" {
		esPassword = "demo"
	}

	// / create endpoint
	endpoint := fmt.Sprintf("http://%s:%s/%s/_doc", esHost, esPort, currentTime)

	tr := &http.Transport{}
	client := &http.Client{Transport: tr}

	reqBody, err := json.Marshal(body)

	req, err := http.NewRequest(http.MethodPost, endpoint, bytes.NewBuffer(reqBody))
	if err != nil {
		return fmt.Errorf("Got error %s", err.Error())
	}

	req.SetBasicAuth(esUsername, esPassword)
	req.Header.Add("Content-Type", "application/json")

	// // Send log to ElasticSearch
	// resp, err := client.Post(
	// 	endpoint,
	// 	"application/json",
	// 	bytes.NewBuffer(reqBody),
	// )

	// if err != nil {
	// 	return err
	// }

	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	var result map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&result)
	return c.JSON(http.StatusOK, result)
}
